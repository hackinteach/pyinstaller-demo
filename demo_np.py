import numpy as np
import sys
import signal 


def signal_handler(sig, frame):
            print('Gracefully shutting down...')
            sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    string = input("Numbers (comma separated) :")
    try:
        mean = np.mean(np.array(string.split(",")).astype(int))
        print("Mean =", mean)
    except Exception as e:
        print("Error: ", e)
    print("Press ctrl+c to quit")