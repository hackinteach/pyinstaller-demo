import sys
import signal 

def echo(msg):
    print(msg)

def signal_handler(sig, frame):
            print('Gracefully shutting down...')
            sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    msg = input("Message: ")
    echo(msg)
    print("Press ctrl-c to exit")
    signal.pause()
