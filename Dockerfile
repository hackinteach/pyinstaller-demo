FROM python:3.7.4-windowsservercore-1809
WORKDIR /src
RUN pip install --upgrade pip
RUN pip install pyinstaller
CMD ['pyinstaller', '--onefile']